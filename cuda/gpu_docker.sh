#!/bin/bash

if [ "$1" == "-h" ] ; then
    echo 'Usage: Control Docker Access to GPU easily [-h]

example: start a container with grahama/tf:latest tied to dev0 gpu
    root@fou:~/code/numerai# gpu_docker 0 grahama/tf:latest

example: start a container with grahama/numerai tied to both GPUs and shared volume
    root@fou:~/code/numerai# gpu_docker all -v /root/code/numerai/shared/:/root/shared/ grahama/numerai
    '
    exit 0
fi

set -e

export CUDA_SO=$(\ls /usr/lib/x86_64-linux-gnu/libcuda* | \
                    xargs -I{} echo '-v {}:{}')
export DEVICES="--device /dev/nvidiactl:/dev/nvidiactl --device /dev/nvidia-uvm:/dev/nvidia-uvm"


# hardcoded to split devices
if [[ "$1" = "0" ]]; then
    export DEVICES="--device /dev/nvidia0:/dev/nvidia0 $DEVICES"
    echo 'using nvidia0 with image:' $4
elif [[ "$1" = "1" ]]; then
    export DEVICES="--device /dev/nvidia1:/dev/nvidia1 $DEVICES"
    echo 'using nvidia1 with image:' $4
elif [[ "$1" = "cpu" ]]; then
    export DEVICES=""
    echo 'using cpu with image:' $4
elif [[ "$1" = "all" ]]; then
    export DEVICES=$(\ls /dev/nvidia* | \xargs -I{} echo '--device {}:{}')
    echo "using all gpu's with image:" $4
fi
# shift inputs
shift

docker run -it $CUDA_SO $DEVICES $@
